with Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Integer_Text_IO;
with Ada.Containers.Generic_Array_Sort;


procedure Main is
   use Ada;
   Input_Filename : String  := "2017_day_4_input.txt";
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   
   function Check_For_Duplicates(Input_String: String) return Boolean is
      Passphrase : String (Input_String'First .. Input_String'Last+2);
      Word_Start : Positive := Passphrase'First;
      Word_End   : Positive := 1;
   begin      
      Passphrase(Input_String'First+1 .. Input_String'Last+1) := Input_String;
      Passphrase(Passphrase'Last) := ' ';
      Passphrase(Passphrase'First) := ' ';
      
      for I in Passphrase'First+1 .. Passphrase'Last loop
         if Passphrase(I) = ' '  then 
            Word_End := I;            
            declare
               Word: String := Passphrase(Word_Start .. Word_End);
               Word_Length: Integer := Word_End - Word_Start;
               
               Passphrase_Copy: String := 
                 Passphrase(Passphrase'First .. Passphrase'First+Word_Start-1) 
                 & Passphrase(Passphrase'First+Word_End-1 .. Passphrase'Last);
            begin               
               if Strings.Fixed.Count(Passphrase_Copy, Word) /= 0 then
                  return True; -- duplicate found
               end if;
            end;
            Word_Start := I;
         end if;
      end loop;
      
      return False; -- no duplicate found
   end Check_For_Duplicates;
   
   procedure String_Sort is
     new Ada.Containers.Generic_Array_Sort(Positive, Character, String);
   
   function Check_For_Anagrams(Input_String: String) return Boolean is      
      Passphrase : String (Input_String'First .. Input_String'Last+2);
      Word_Start : Integer := 0;
      Word_End   : Integer := 0;
   begin
      
      Passphrase(Input_String'First+1 .. Input_String'Last+1) := Input_String;
      Passphrase(Passphrase'Last) := ' ';
      Passphrase(Passphrase'First) := ' ';
      
      -- Sort all words into alphabetical order
      Word_Start := Passphrase'First;
      for I in Passphrase'First+1 .. Passphrase'Last loop
         
         if Passphrase(I) = ' ' then 
            Word_End := I;
            
            String_Sort(Passphrase(Word_Start+1 .. Word_End-1));
            Word_Start := I;
         end if;
      end loop;
      
      -- Search for duplicate words in string
      Word_Start := Passphrase'First;
      for I in Passphrase'First+1 .. Passphrase'Last loop
         
         if Passphrase(I) = ' '  then 
            Word_End := I;
            
            declare
               Word: String := Passphrase(Word_Start .. Word_End);
               Word_Length: Integer := Word_End - Word_Start;
               
               Passphrase_Copy: String 
                 := Passphrase(Passphrase'First .. Passphrase'First+Word_Start-1) 
                   & Passphrase(Passphrase'First+Word_End-1 .. Passphrase'Last);
            begin
               if Strings.Fixed.Count(Passphrase_Copy, Word) /= 0 then
                  return True; -- duplicate found
               end if;
            end;
            
            Word_Start := I;
         end if;
      end loop; 
      
      return False; -- no duplicate found
   end Check_For_Anagrams;
   
   procedure High_Entropy_Passphrases(Part_1: out Integer; Part_2: out Integer; Filename: in String) is            
      Input_File    : Text_IO.File_Type;
   begin
      Part_1 := 0;
      Part_2 := 0;
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Filename);
      
      -- Part 1
      while not Text_IO.End_Of_File(Input_File) loop
         if Check_For_Duplicates(Text_IO.Get_Line(Input_File)) = False then
            Part_1 := Part_1 + 1;
         end if;
      end loop;
      
      -- Part 2
      Text_IO.Reset(Input_File);
      while not Text_IO.End_Of_File(Input_File) loop
         if Check_For_Anagrams(Text_IO.Get_Line(Input_File)) = False then
            Part_2 := Part_2 + 1;
         end if;
      end loop;
      
      Text_IO.Close(Input_File);
   end High_Entropy_Passphrases;
   
begin  
   
   High_Entropy_Passphrases(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put("Answer Part 2:" & Integer'Image(Part_2));   
   
end Main;

