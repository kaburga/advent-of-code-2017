with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Command_Line;
with Ada.Sequential_IO;

procedure Main is
   use Ada;
   type Number_List is array (Natural range <>) of Integer;
   Input_Filename : String  := "2017_day_2_input.txt";
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   
   function Calculate_Checksum(Input_File: out Text_IO.File_Type) return Integer is
      Value : Integer := 0;
      Max   : Integer := Integer'First;
      Min   : Integer := Integer'Last;
      Sum   : Integer := 0;
   begin -- Calculate_Checksum
      while not Text_IO.End_Of_File(Input_File) loop           
         Integer_Text_IO.Get(Input_File, Value);
         
         if Value > Max then Max := Value; end if;
         if Value < Min then Min := Value; end if;
         
         if Text_IO.End_Of_Line(Input_File) then
            Sum := Sum + (Max - Min);
            Max := Integer'First;
            Min := Integer'Last;
         end if;
      end loop;
      Text_IO.Reset(Input_File);
      
      return Sum;
   end Calculate_Checksum;
   
   function Count_Row_Values(Input_File: in out Text_IO.File_Type; Line_Nr: in Positive) return Integer is            
      Value          : Integer := 0;
      Numbers_In_Row : Integer := 0;
   begin -- Compute_Row_Sum      
         -- Go to line
      Text_IO.Reset(Input_File);
      Text_IO.Set_Line(Input_File, Text_IO.Count(Line_Nr));
      
      -- Count number of values on line
      while not Text_IO.End_Of_Line(Input_File) loop
         Integer_Text_IO.Get(Input_File, Value);
         Numbers_In_Row := Numbers_In_Row + 1;
      end loop;
      
      -- Reset to same line
      Text_IO.Reset(Input_File);
      Text_IO.Set_Line(Input_File, Text_IO.Count(Line_Nr));
      
      return Numbers_In_Row;
   end Count_Row_Values;
   
   function Find_Divisible(Data_File: in Text_IO.File_Type; Numbers_In_Row: in Integer; Divisible_Found: out Boolean) return Integer is      
      Current_Row_Numbers: Number_List(1 .. Numbers_In_Row);
   begin -- Find_Divisible      
      for I in 1 .. Numbers_In_Row loop 
         Integer_Text_IO.Get(Data_File, Current_Row_Numbers(I));
      end loop;
      
      for I in Current_Row_Numbers'First .. Current_Row_Numbers'Last loop
         for J in Current_Row_Numbers'First .. Current_Row_Numbers'Last loop
            
            if I /= J then
               if Current_Row_Numbers(I) mod Current_Row_Numbers(J) = 0 then
                  Divisible_Found := True;
                  return ( Current_Row_Numbers(I) / Current_Row_Numbers(J));
               end if;
            end if;
            
         end loop;
      end loop;
      
      Divisible_Found := False;
      return -1; -- No divisible found
   end Find_Divisible;
   
   procedure Corruption_Checksum(Part_1 : out Integer; Part_2 : out Integer; Filename : in String) is      
      Input_File     : Text_IO.File_Type;
      Value          : Integer := 0;
      Row_Count      : Integer := 1;
      Numbers_In_Row : Integer := 0;
   begin -- Corruption_Checksum
      Part_1 := 0;
      Part_2 := 0;
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Filename);
      -- Part 1
      Part_1 := Calculate_Checksum(Input_File);
      
      -- Part 2      
      while not Text_IO.End_Of_File(Input_File) loop
         Numbers_In_Row := Count_Row_Values(Input_File, Row_Count);
         
         declare
            Divisible_Found: Boolean := False;
            Result: Integer := 0;
         begin
            Result := Find_Divisible(Input_File, Numbers_In_Row, Divisible_Found);
            if Divisible_Found = False then
               Text_IO.Close(Input_File);
               Text_IO.Put_Line("Bad input file");
               return;
            elsif Divisible_Found = True then
               Part_2 := Part_2 + Result;
            end if;
         end;
         
         Row_Count := Row_Count + 1;
      end loop;
      Text_IO.Close(Input_File);
   end Corruption_Checksum;
   
begin -- Main
   Corruption_Checksum(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put("Answer Part 2:" & Integer'Image(Part_2));   
end Main;

