with Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Integer_Text_IO;
with Ada.Unchecked_Deallocation;

procedure Main is
   use Ada;
   type Node;
   type Access_Node is access Node;
   type Program_Array is array(Positive range <>) of Access_Node;
   type Program_List is
      record
         Head      : Access_Node := null;
         Tail      : Access_Node := null;
      end record;
   subtype Program_Name_Length is Natural range 0 .. 10;
   
   type Node(Name_Length: Program_Name_Length  := 0) is
      record
         Name                 : String(1 .. Name_Length) := (others => ' ');
         Node_Weight          : Natural                  := 0;
         Total_Weight         : Natural                  := 0;
         Num_Of_Programs_Held : Integer                  := 0;
         Held_Above_List      : Program_List;
         Next                 : Access_Node              := null;
         Previous             : Access_Node              := null;
         On_Top_Of            : Access_Node              := null;
      end record;
   
   Input_Filename : String  := "2017_day_7_input.txt";
   -- Input_Filename : String  := "test.txt";
   
   procedure Store_Programs_Held_Above(Program: in not null Access_Node;
                                       String_Of_Program_Names: in String;
                                       Num_Of_Programs_Held: in Positive)
   is
      Pos_1     : Positive := String_Of_Program_Names'First;
      Pos_2     : Positive := String_Of_Program_Names'First;
      Pos_Above : Positive := 1;
      New_Node_Pointer : Access_Node := Program.Held_Above_List.Head;
   begin -- Store_Programs_Held_Above
      Program.Held_Above_List.Head := New_Node_Pointer;
      Program.Held_Above_List.Tail := New_Node_Pointer;
      for Index in String_Of_Program_Names'First .. String_Of_Program_Names'Last loop
         if String_Of_Program_Names(Index) = ',' then
            Pos_2 := Index-1;
            New_Node_Pointer := new Node(String_Of_Program_Names(Pos_1 .. Pos_2)'Length);
            New_Node_Pointer.Name  := String_Of_Program_Names(Pos_1 .. Pos_2);            
            
            if Program.Held_Above_List.Head = null then
               Program.Held_Above_List.Tail := New_Node_Pointer;
               Program.Held_Above_List.Head := New_Node_Pointer;
            elsif Program.Held_Above_List.Tail /= null then
               Program.Held_Above_List.Tail.Next := New_Node_Pointer;
               New_Node_Pointer.Previous := Program.Held_Above_List.Tail;
               Program.Held_Above_List.Tail := New_Node_Pointer;
            end if;
            
            New_Node_Pointer.On_Top_Of := Program;
            
            Pos_Above := Pos_Above + 1;
            Pos_1 := Index + 2;
         elsif Index = String_Of_Program_Names'Last then
            Pos_2 := Index;
            New_Node_Pointer := new Node(String_Of_Program_Names(Pos_1 .. Pos_2)'Length);
            New_Node_Pointer.Name := String_Of_Program_Names(Pos_1 .. Pos_2);
            New_Node_Pointer.On_Top_Of := Program;
            
            if Program.Held_Above_List.Head = null then
               Program.Held_Above_List.Tail := New_Node_Pointer;
               Program.Held_Above_List.Head := New_Node_Pointer;
            end if;
            if Program.Held_Above_List.Tail /= null then
               Program.Held_Above_List.Tail.Next := New_Node_Pointer;
               New_Node_Pointer.Previous := Program.Held_Above_List.Tail;
               Program.Held_Above_List.Tail := New_Node_Pointer;
            end if;
            if Num_Of_Programs_Held /= Pos_Above then
               raise Program_Error;
            end if;
         end if;
      end loop;
   end Store_Programs_Held_Above;
   
   function Count_Number_Of_Programs_Held_Above(String_Of_Program_Names : in String) return Natural is
      Num_Of_Programs : Natural := 0;
   begin -- Count_Number_Of_Programs_Held_Above
      for Index in String_Of_Program_Names'First .. String_Of_Program_Names'Last loop
         if String_Of_Program_Names(Index) = ',' then
            Num_Of_Programs := Num_Of_Programs + 1;
         elsif Index = String_Of_Program_Names'Last then
            Num_Of_Programs := Num_Of_Programs + 1;
         end if;
      end loop;
      return Num_Of_Programs;
   end Count_Number_Of_Programs_Held_Above;
   
   procedure String_To_Program(Row_Data: in String; Program: out Access_Node) is
      Pos_1                : Integer     := 0;
      Pos_2                : Integer     := 0;
      Num_Of_Programs_Held : Integer     := 0;
      Weight               : Natural     := 0;
   begin -- String_To_Program
         -- find weight
      Pos_1 := Strings.Fixed.Index(Row_Data, "(");
      Pos_2 := Strings.Fixed.Index(Row_Data, ")");
      Weight := Integer'Value(Row_Data(Pos_1+1 .. Pos_2-1));
      declare
         -- name of program
         Program_Name: String := Row_Data(Row_Data'First .. Pos_1-2);
      begin
         -- find position of '->' if any, store names of programs held above
         Pos_1 := Strings.Fixed.Index(Row_Data, ">");
         if Pos_1 >= Row_Data'First then
            Num_Of_Programs_Held := Count_Number_Of_Programs_Held_Above(Row_Data(Pos_1 .. Row_Data'Last));
         end if;
         Program := new Node(Name_Length => Program_Name'Length);
         Program.Name := Program_Name;
         Program.Num_Of_Programs_Held := Num_Of_Programs_Held;
         Program.Node_Weight := Weight;
      end;
      -- find and store all names of programs held above this one
      if Num_Of_Programs_Held > 0 then
         Store_Programs_Held_Above(Program, Row_Data(Pos_1+2 .. Row_Data'Last), Num_Of_Programs_Held);
      end if;
   end String_To_Program;
   
   procedure Free is new Unchecked_Deallocation(Node, Access_Node);
   
   procedure Search_And_Replace_Placeholders(Array_Of_Programs: in Program_Array; Program: in out Access_Node) is
      Pointer : Access_Node := null;
   begin -- Search_Tower_For_Program
      
      for I in Array_Of_Programs'First .. Array_Of_Programs'Last loop
         if Array_Of_Programs(I).Num_Of_Programs_Held > 0 then            
            Pointer := Array_Of_Programs(I).Held_Above_List.Head;
            
            while Pointer /= null loop
               if Pointer.Name = Program.Name then
                  Program.On_Top_Of := Pointer.On_Top_Of;
                  
                  if Pointer = Array_Of_Programs(I).Held_Above_List.Head and
                    Pointer = Array_Of_Programs(I).Held_Above_List.Tail then
                     Array_Of_Programs(I).Held_Above_List.Head := Program;
                     Array_Of_Programs(I).Held_Above_List.Tail := Program;
                  elsif Pointer = Array_Of_Programs(I).Held_Above_List.Head then
                     Array_Of_Programs(I).Held_Above_List.Head := Program;
                  elsif Pointer = Array_Of_Programs(I).Held_Above_List.Tail then
                     Array_Of_Programs(I).Held_Above_List.Tail := Program;
                  end if;
                  if Pointer.Previous /= null then
                     Pointer.Previous.Next := Program;
                     Program.Previous := Pointer.Previous;
                  end if;
                  if Pointer.Next /= null then
                     Pointer.Next.Previous := Program;
                     Program.Next := Pointer.Next;
                  end if;
                  Free(Pointer);
                  return;
               else
                  Pointer := Pointer.Next;
               end if;
            end loop;
         end if;
      end loop;
      return;
   end Search_And_Replace_Placeholders;
   
   function Find_Bottom_Of_Tower(Program: in Access_Node) return Access_Node is
      Search_Pointer : Access_Node := Program;
   begin -- Find_Bottom_Of_Tower
      while Search_Pointer.On_Top_Of /= null loop
         Search_Pointer := Search_Pointer.On_Top_Of;
      end loop;
      return Search_Pointer;
   end Find_Bottom_Of_Tower;
   
   function Build_Tower_Of_Programs(Array_Of_Programs : in Program_Array) return Access_Node is
      Program_Pointer : Access_Node := null;
   begin -- Build_List_Of_Programs
      for I in Array_Of_Programs'First .. Array_Of_Programs'Last loop
         Program_Pointer := Array_Of_Programs(I);
         Search_And_Replace_Placeholders(Array_Of_Programs, Program_Pointer);
      end loop;
      Program_Pointer := Find_Bottom_Of_Tower(Array_Of_Programs(Array_Of_Programs'First));
      return Program_Pointer;
   end Build_Tower_Of_Programs;
   
   function Calculate_Tower_Weight(Root : in out Access_Node) return Positive is
      Node_Above_Pointer: Access_Node; 
   begin  -- Calculate_Tower_Weight
      if Root.Num_Of_Programs_Held > 0 then
         Node_Above_Pointer := Root.Held_Above_List.Head;
         while Node_Above_Pointer /= null loop
            Root.Total_Weight := Root.Total_Weight + Calculate_Tower_Weight(Node_Above_Pointer);
            Node_Above_Pointer := Node_Above_Pointer.Next;
         end loop;
         Root.Total_Weight := Root.Total_Weight + Root.Node_Weight;
         return Root.Total_Weight;
      else
         Root.Total_Weight := Root.Node_Weight;
         return Root.Total_Weight;
      end if;      
   end Calculate_Tower_Weight;
   
   
   function Check_If_Nodes_Held_Above_Node_Is_Same_Weight(First: in Integer; Last: in Integer; Member: in Access_Node) return Boolean is      
      Member_Total_Weight   : Integer := Member.Total_Weight;
      Unbalanced_Disc_Found : Boolean := False;
   begin -- Check_If_Nodes_Held_Above_Node_Is_Same_Weight
      if Member.Num_Of_Programs_Held > 0 then
         declare
            First_Weight_Above      : Integer     := Member.Held_Above_List.Head.Total_Weight;
            Held_Above_Node_Pointer : Access_Node := Member.Held_Above_List.Head;
         begin
            Unbalanced_Disc_Found := True;
            while Held_Above_Node_Pointer /= null loop
               if First_Weight_Above /= Held_Above_Node_Pointer.Total_Weight then
                  Unbalanced_Disc_Found := False;
                  exit;
               end if;
               Held_Above_Node_Pointer := Held_Above_Node_Pointer.Next;
            end loop;
         end;
      end if;
      
      return Unbalanced_Disc_Found ;
   end Check_If_Nodes_Held_Above_Node_Is_Same_Weight;
   
   function Needed_Weight_Adjustment_Of_Unbalanced_Disc(Root : in out Access_Node;
                                                        Unbalanced_Disc_Found: in out Boolean) return Natural is
      Node_Above_Pointer: Access_Node;
      New_Node_Weight: Integer := 0;
      Total_Tower_Weight_Array: array (1 .. Root.Num_Of_Programs_Held) of Integer;
      Node_Array: array (1 .. Root.Num_Of_Programs_Held) of Access_Node;
      First: Integer := 0;
      Last: Integer := 0;
   begin -- Needed_Weight_Adjustment_Of_Unbalanced_Disc
      if Root.Num_Of_Programs_Held > 0 then
         
         Node_Above_Pointer := Root.Held_Above_List.Head;
         
         for I in Total_Tower_Weight_Array'First .. Total_Tower_Weight_Array'Last loop
            Total_Tower_Weight_Array(I) := Node_Above_Pointer.Total_Weight;
            Node_Array(I) := Node_Above_Pointer;
            Node_Above_Pointer := Node_Above_Pointer.Next;
         end loop;
         
         for I in Total_Tower_Weight_Array'First+1 .. Total_Tower_Weight_Array'Last-1 loop
            
            First := Total_Tower_Weight_Array(Total_Tower_Weight_Array'First);
            Last := Total_Tower_Weight_Array(Total_Tower_Weight_Array'Last);
            
            if First /= Total_Tower_Weight_Array(I) and Last /= Total_Tower_Weight_Array(I) then
               
               Unbalanced_Disc_Found := Check_If_Nodes_Held_Above_Node_Is_Same_Weight(First, Last, Node_Array(I));
               
               if Unbalanced_Disc_Found = True then
                  New_Node_Weight := Node_Array(I).Node_Weight - (First - Total_Tower_Weight_Array(I));
                  return New_Node_Weight;
               end if;
               
               
            elsif First /= Total_Tower_Weight_Array(I) and Last = Total_Tower_Weight_Array(I) then
               
               Unbalanced_Disc_Found := Check_If_Nodes_Held_Above_Node_Is_Same_Weight(First, Last, Node_Array(Node_Array'First));
               
               if Unbalanced_Disc_Found = True then
                  New_Node_Weight := Node_Array(Node_Array'First).Node_Weight - (First - Total_Tower_Weight_Array(I));
                  return New_Node_Weight;
               end if;
               
            elsif First = Total_Tower_Weight_Array(I) and Last /= Total_Tower_Weight_Array(I) then
               
               Unbalanced_Disc_Found := Check_If_Nodes_Held_Above_Node_Is_Same_Weight(First, Last, Node_Array(Node_Array'Last));
               
               if Unbalanced_Disc_Found = True then
                  New_Node_Weight := Node_Array(Node_Array'Last).Node_Weight - (Last - Total_Tower_Weight_Array(I));
                  return New_Node_Weight;
               end if;                  
               
            end if;
         end loop;
         
         Node_Above_Pointer := Root.Held_Above_List.Head;
         while Node_Above_Pointer /= null loop
            New_Node_Weight := Needed_Weight_Adjustment_Of_Unbalanced_Disc(Node_Above_Pointer, Unbalanced_Disc_Found);
            if Unbalanced_Disc_Found = True then
               return New_Node_Weight;
            end if;
            Node_Above_Pointer := Node_Above_Pointer.Next;
         end loop;
         
      end if;
      Unbalanced_Disc_Found := False;
      return New_Node_Weight;
   end Needed_Weight_Adjustment_Of_Unbalanced_Disc;
   
   procedure Recursive_Circus(Input_Filename: String)
   is
      Input_File: Text_IO.File_Type;
      Num_Of_Lines: Natural := 0;
   begin -- Recursive_Circus
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Input_Filename);
      
      while not Text_IO.End_Of_File(Input_File) loop
         Num_of_Lines := Num_of_Lines + 1;
         --Text_IO.Skip_Line;
         declare
            Line : String := Ada.Text_IO.Get_Line(Input_File);
         begin
            null;
         end;
      end loop;      
      Text_IO.Reset(Input_File);
      declare
         Array_Of_Programs: Program_Array(1 .. Num_Of_Lines);
         Root_Of_Tower: Access_Node := null;
         Total_Weight: Positive := 1;
         Adjusted_Node_Weight: Natural := 0;
         Unbalanced_Disc_Found: Boolean := False;
      begin
         for I in 1 .. Num_Of_Lines loop
            String_To_Program(Text_IO.Get_Line(Input_File), Array_Of_Programs(I));
         end loop;
         Root_Of_Tower := Build_Tower_Of_Programs(Array_Of_Programs);
         Text_IO.Put_Line("Bottom program: " & Root_Of_Tower.Name);
         Total_Weight := Calculate_Tower_Weight(Root_Of_Tower);
         Text_IO.Put_Line("Total weight" & Integer'Image(Total_Weight));
         Adjusted_Node_Weight := Needed_Weight_Adjustment_Of_Unbalanced_Disc(Root_Of_Tower, Unbalanced_Disc_Found);
         
         if Unbalanced_Disc_Found = True then
            Text_IO.Put_Line("Adjusted weight of node: " & Natural'Image(Adjusted_Node_Weight));
         else
            Text_IO.Put_Line("No disc found");
         end if;
         
      end;
      Text_IO.Close(Input_File);
   end Recursive_Circus;
   
begin -- Main
   
   Recursive_Circus(Input_Filename);
   
end Main;

