with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure Main is
   use Ada;
   type Memory_Bank_16 is array (1 .. 16) of Integer with Default_Component_Value => 0;
   type Memory_Bank_Array is array(Positive range <>) of Memory_Bank_16;
   Input_Filename : String  := "2017_day_6_input.txt";
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   
   procedure Find_Previous_State(Part_1 : out Integer; Part_2 : out Integer; Previous_States : in out Memory_Bank_Array) is     
      Memory         : Memory_Bank_16;
      Most_Blocks    : Positive;
      Index          : Positive;
      Redist_Bank    : Integer := 0;
      Cycles_Between : Integer := 0;
   begin      
      Memory := Previous_States(1);
      
      for Cycle in Previous_States'First .. Previous_States'Last loop         
         --Memory := Previous_States(Cycle);
         Most_Blocks := Memory'First;
         
         for I in Memory'First+1 .. Memory'Last loop
            if Memory(Most_Blocks) < Memory(I) then
               Most_Blocks := I;
            end if;
         end loop;
         
         Redist_Bank := Memory(Most_Blocks);
         Memory(Most_Blocks) := 0;
         Index := Most_Blocks;
         
         while Redist_Bank > 0 loop
            if Index = Memory'Last then
               Index := Memory'First;
            else
               Index := Index + 1;
            end if;
            Memory(Index) := Memory(Index) + 1;
            Redist_Bank := Redist_Bank - 1;
         end loop;
         
         for I in Previous_States'First .. Cycle loop
            if Previous_States(I) = Memory then
               Cycles_Between := Cycle+1 - I;
               
               Part_1 := Cycle;
               Part_2 := Cycles_Between;
               return;
            end if;
         end loop;
         
         if Cycle+1 > Previous_States'Last then
            exit;
         end if;
         
         Previous_States(Cycle+1) := Memory;
      end loop;
      
      return;
   end Find_Previous_State;
   
   procedure Memory_Reallocation(Part_1: out Integer; Part_2: out Integer; Input_Filename: in String) is      
      Input_File      : Text_IO.File_Type;
      Memory          : Memory_Bank_16;
      Previous_Memory : Memory_Bank_Array(1 .. 10000);
      Total_Cycles    : Natural := 0;      
   begin
      Part_1 := 0;
      Part_2 := 0;
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Input_Filename);
      
      for I in Memory_Bank_16'Range loop
         -- don't read if file somehow does not contain 16 values
         if not Text_IO.End_Of_Line(Input_File) then
            Integer_Text_IO.Get(Input_File, Memory(I) );
         end if;
      end loop;
      
      Previous_Memory(1) := Memory;
      Find_Previous_State(Part_1, Part_2, Previous_Memory);
      
      Text_IO.Close(Input_File);
   end Memory_Reallocation;
   
begin -- Main
   
   Memory_Reallocation(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put("Answer Part 2:" & Integer'Image(Part_2));      
   
end Main;

