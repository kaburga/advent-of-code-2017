with Ada.Text_IO;
use Ada;

package body Inverse_Captcha_Solver is

   type Digit_List is array (Natural range <>) of Integer range 0 .. 9;

   function Format (S: in String) return Digit_List is
      L: Digit_List (S'Range);
   begin -- Format
      for I in S'Range loop
         L(I) := Integer'Value((1 => S(I)));
      end loop;
      return L;
   end Format;      
   
   function Read_Input_Data(Filename: String) return Digit_List is      
      Input_File: Text_IO.File_Type;
   begin -- Read_Input_Data
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Filename);
      declare
         Input_Data: String := Text_IO.Get_Line(Input_File);
         Formatted_Input_Data: Digit_List := Format(Input_Data);
      begin
         Text_IO.Close(Input_File);
         return Formatted_Input_Data;
      end;
   end Read_Input_Data;
   
   procedure Inverse_Captcha(Part_1: out Integer; Part_2: out Integer; Input_Filename: in String) is   
      Puzzle_Input   : Digit_List := Read_Input_Data(Input_Filename);
      Half_Nr_Digits : Integer    := (Puzzle_Input'Length)/2;
      Halfway        : Integer    := 0;
   begin -- Inverse_Captcha
      Part_1 := 0;
      Part_2 := 0;      
      
      -- Part 1
      for I in Puzzle_Input'First .. Puzzle_Input'Last-1 loop
         if Puzzle_Input(I) = Puzzle_Input(I+1) then
            Part_1 := Part_1 + Puzzle_Input(I);
         end if;
      end loop;
      
      if Puzzle_Input(Puzzle_Input'Last) = Puzzle_Input(Puzzle_Input'First) then
         Part_1 := Part_1 + Puzzle_Input(Puzzle_Input'First);
      end if;
      
      -- Part 2
      for I in Puzzle_Input'Range loop
         Halfway := (I + Half_Nr_Digits) mod Puzzle_Input'Length;
         if Halfway = 0 then Halfway := Puzzle_Input'Last; end if;
         
         if Puzzle_Input(I) = Puzzle_Input(Halfway) then
            Part_2 := Part_2 + Puzzle_Input(I);
         end if;
      end loop;      
   end Inverse_Captcha;

end Inverse_Captcha_Solver;

