with Ada.Text_IO;
with Inverse_Captcha_Solver;
use Inverse_Captcha_Solver;

procedure Main is
   use Ada;
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   Input_Filename : String  := "2017_day_1_input.txt";   
begin -- Main
   Inverse_Captcha(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put_Line("Answer Part 2:" & Integer'Image(Part_2));
end Main;

