with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure Main is
   use Ada;
   type Square_Spiral is array( Integer range <>, Integer range <>) of Integer;
   Input_Filename : String  := "2017_day_3_input.txt";
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   
   function Calculate_Steps(Puzzle_Input: in Integer) return Integer is      
      X            : Integer := 1; -- X*X cirle
      X_2          : Integer := 0;
      Num_Of_Steps : Integer := 0;
      Corner       : Integer := 0;
      Row_Middle   : Integer := 0;
   begin -- Calculate_Steps
      if Puzzle_Input = 1 then  
         return Num_Of_Steps;
      else          
      Cirle_Lvl:
         loop            
            X := X + 2;
            X_2 := (X**2);
            if Puzzle_Input <= X_2 then               
               for I in 1 .. 4 loop
                  Corner := (X_2 - ((X-1) * I));                   
                  if Puzzle_Input >= Corner then
                     Row_Middle := Corner + (X - 1)/2;
                     Num_Of_Steps := abs(Puzzle_Input - Row_Middle) + (X-1)/2;
                     exit Cirle_Lvl;
                  end if; 
               end loop;
            end if;
         end loop Cirle_Lvl;         
      end if;          
      
      return Num_Of_Steps;
   end Calculate_Steps;
   
   function Square_Spiral_Search (Value: in Integer; Square_Size: in Integer) return Integer is      
      Memory: Square_Spiral (1 .. Square_Size, 1 .. Square_Size) := (1 .. Square_Size => (others => 0));
      Curr_Size: Integer := 3;
      X: Integer := 0;
      Y: Integer := 0;      
   begin      
      X := (Square_Size-1)/2 + 1;
      Y := (Square_Size-1)/2 + 1;
      Memory(X, Y) := 1;
      
   Square_Loop:
      loop
         if Curr_Size > Square_Size then exit; end if;
         X := X + 1;
         
         -- Right side
         for I in 1 .. Curr_Size - 2 loop            
            Memory(X, Y) := Memory(X, Y-1) + Memory(X-1, Y-1) + Memory(X-1, Y) + Memory(X-1, Y+1);            
            if Memory(X, Y) > Value then return Memory(X, Y); end if;
            Y := Y + 1;
         end loop;
         
         -- Corner
         Memory(X, Y) := Memory(X, Y-1) + Memory(X-1, Y-1);         
         if Memory(X, Y) > Value then return Memory(X, Y); end if;
         X := X - 1;
         
         -- Top side
         for I in 1 .. Curr_Size - 2 loop
            Memory(X, Y) := Memory(X+1, Y) + Memory(X+1, Y-1) + Memory(X, Y-1) + Memory(X-1, Y-1);            
            if Memory(X, Y) > Value then return Memory(X, Y); end if;
            X := X - 1;
         end loop;
         
         -- Corner 2
         Memory(X, Y) := Memory(X+1, Y) + Memory(X+1, Y-1);         
         if Memory(X, Y) > Value then return Memory(X, Y); end if;
         Y := Y - 1;
         
         -- Left side
         for I in 1 .. Curr_Size - 2 loop
            Memory(X, Y) := Memory(X, Y+1) + Memory(X+1, Y+1) + Memory(X+1, Y) + Memory(X+1, Y-1);            
            if Memory(X, Y) > Value then return Memory(X, Y); end if;
            Y := Y - 1;
         end loop;
         
         -- Corner 3
         Memory(X, Y) := Memory(X, Y+1) + Memory(X+1, Y+1);         
         if Memory(X, Y) > Value then return Memory(X, Y); end if;
         X := X + 1;
         
         -- Bottom side
         for I in 1 .. Curr_Size - 2 loop
            Memory(X, Y) := Memory(X-1, Y) + Memory(X-1, Y+1) + Memory(X, Y+1) + Memory(X+1, Y+1);            
            if Memory(X, Y) > Value then return Memory(X, Y); end if;
            X := X + 1;
         end loop;
         
         -- Corner 4
         Memory(X, Y) := Memory(X-1, Y) + Memory(X-1, Y+1) + Memory( X, Y+1);
         if Memory(X, Y) > Value then return Memory(X, Y); end if;
         
         Curr_Size := Curr_Size + 2;
      end loop Square_Loop;
      
      return -1;
   end Square_Spiral_Search;
   
   function Find_Larger(Puzzle_Input : Integer) return Integer is      
      Answer      : Integer := 0;
      Square_Size : Integer := 9; -- X*X square
   begin
      if Puzzle_Input = 1 then
         return 2;
      else          
         loop            
            Answer := Square_Spiral_Search(Puzzle_Input, Square_Size);
            if Answer > Puzzle_Input then
               return Answer;
            end if;
            Square_Size := Square_Size + 2;            
         end loop;         
      end if;
   end Find_Larger;
   
   procedure Spiral_Memory(Part_1 : out Integer; Part_2 : out Integer; Filename : in String) is                  
      Input_File   : Text_IO.File_Type;
      Puzzle_Input : Integer := 0;
   begin -- Spiral_Memory
      Part_1 := 0;
      Part_2 := 0;
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Filename);      
      Integer_Text_IO.Get(Input_File, Puzzle_Input);
      
      -- Part 1      
      Part_1 := Calculate_Steps(Puzzle_Input);      
      -- Part 2
      Part_2 := Find_Larger(Puzzle_Input);
      
      Text_IO.Close(Input_File);
   end Spiral_Memory;
   
begin -- Main
   Spiral_Memory(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put("Answer Part 2:" & Integer'Image(Part_2));   
end Main;

