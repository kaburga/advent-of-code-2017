with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure Main is
   use Ada;
   Input_Filename : String  := "2017_day_5_input.txt";
   Part_1         : Integer := 0;
   Part_2         : Integer := 0;
   
   function Count_Steps_Part_1(Input_File : in out Text_IO.File_Type) return Integer is      
      Jump_Value  : Integer := 0;
      Num_Of_Rows : Integer := 0;
      Jump_Total  : Integer := 0;
   begin
        -- Count nr of lines
      while not Text_IO.End_Of_File(Input_File) loop
         Integer_Text_IO.Get(Input_File, Jump_Value);
         Num_Of_Rows := Num_Of_Rows + 1;
      end loop;

      Text_IO.Reset(Input_File);
      
      declare
         Jump_Array: array (1 .. Num_Of_Rows) of Integer; 
         Index : Integer := Jump_Array'First;
      begin         
         for I in Jump_Array'Range loop
            Integer_Text_IO.Get(Input_File, Jump_Array(I));
         end loop;
         
         while Index >= Jump_Array'First and then Index <= Jump_Array'Last loop
            Jump_Value := Jump_Array(Index);
            Jump_Array(Index) := Jump_Array(Index) + 1;
            Index := Index + Jump_Value;
            Jump_Total := Jump_Total + 1;
         end loop;
      end;
      
      return Jump_Total;
   end Count_Steps_Part_1;
   
   function Count_Steps_Part_2(Input_File : in out Text_IO.File_Type) return Integer is
      Jump_Value  : Integer := 0;
      Num_Of_Rows : Integer := 0;
      Jump_Total  : Integer := 0;      
   begin
      -- Count nr of lines
      while not Text_IO.End_Of_File(Input_File) loop
         Integer_Text_IO.Get(Input_File, Jump_Value);
         Num_Of_Rows := Num_Of_Rows + 1;
      end loop;
      
      Text_IO.Reset(Input_File);
      
      declare
         Jump_Array: array (1 .. Num_Of_Rows) of Integer; 
         Index : Integer := Jump_Array'First;
      begin         
         for I in Jump_Array'Range loop
            Integer_Text_IO.Get(Input_File, Jump_Array(I));
         end loop;
         
         while Index >= Jump_Array'First and then Index <= Jump_Array'Last loop
            Jump_Value := Jump_Array(Index);
            
            if Jump_Value >= 3 then
               Jump_Array(Index) := Jump_Array(Index) - 1;
            else
               Jump_Array(Index) := Jump_Array(Index) + 1;
            end if;
            
            Index := Index + Jump_Value;
            Jump_Total := Jump_Total + 1;
         end loop;
      end;
      
      return Jump_Total;
   end Count_Steps_Part_2;
   
   procedure Twisty_Trampolines(Part_1: out Integer; Part_2: out Integer; Input_Filename: in String) is
      Input_File: Text_IO.File_Type;
   begin
      Part_1 := 0;
      Part_2 := 0;
      
      Text_IO.Open(File => Input_File,
                   Mode => Text_IO.In_File,
                   Name => Input_Filename);
      
      Part_1 := Count_Steps_Part_1(Input_File);
      
      Text_IO.Reset(Input_File);
      
      Part_2 := Count_Steps_Part_2(Input_File);
      
      Text_IO.Close(Input_File);
   end Twisty_Trampolines;
   
begin -- Main
   
   Twisty_Trampolines(Part_1, Part_2, Input_Filename);
   
   Text_IO.Put_Line("Answer Part 1:" & Integer'Image(Part_1));
   Text_IO.Put("Answer Part 2:" & Integer'Image(Part_2));      
   
end Main;

